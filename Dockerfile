FROM phusion/baseimage


############################### End Consistent base image ##############################
ENV \
    TERM=xterm

# Ensure UTF-8
RUN locale-gen en_US.UTF-8
ENV LANG       en_US.UTF-8
ENV LC_ALL     en_US.UTF-8

## install utilities
RUN \
    apt-get update && apt-get install -y --no-install-recommends \
        wget \
        vim \
        telnet

RUN wget -q http://sensu.global.ssl.fastly.net/apt/pubkey.gpg -O- | apt-key add -
RUN echo "deb http://sensu.global.ssl.fastly.net/apt sensu main" | tee /etc/apt/sources.list.d/sensu.list \
    && apt-get update \
    && apt-get install sensu

COPY \
     docker-src/files / 

RUN \
   find /etc/service -type f -exec chmod +x {} \;

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

